//
//  ViewController.swift
//  openCV
//
//  Created by Master Móviles on 17/11/16.
//  Copyright © 2016 Master Móviles. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var original: UIImage!
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var slider: UISlider!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        imageView.image = UIImage(named: "ray-donovan")
        original = UIImage(named: "ray-donovan")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onOriginalClicked(_ sender: Any) {
        imageView.image = original
    }

    @IBAction func onDetectBordersClicked(_ sender: Any) {
        imageView.image = OpenCVWrapper.detectBorders(of: imageView.image, withThreshold: slider.value);
    }
    
    @IBAction func onConvertToGrayscaleClicked(_ sender: Any) {
        imageView.image = OpenCVWrapper.convertImage(toGrayscale: imageView.image!)
    }

    @IBAction func onSliderValueChanged(_ sender: Any) {
        imageView.image = OpenCVWrapper.detectBorders(of: original, withThreshold: slider.value)
    }
    
    @IBAction func onDetectFacesClicked(_ sender: Any) {
        imageView.image = OpenCVWrapper.detectFaces(of: imageView.image);
    }
}

