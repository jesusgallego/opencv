//
//  OpenCVWrapper.m
//  openCV
//
//  Created by Master Móviles on 17/11/16.
//  Copyright © 2016 Master Móviles. All rights reserved.
//

#import "OpenCVWrapper.h"


@interface OpenCVWrapper ()<CvVideoCameraDelegate>
{
    
}
@end

@implementation OpenCVWrapper
{
    VideoViewController* viewController;
    UIImageView* imageView;
    CvVideoCamera* videoCamera;
    float threshold;
}

+ (UIImage *) convertImageToGrayscale: (UIImage *) image
{
    cv::Mat imageMat;
    UIImageToMat(image, imageMat);
    
    if (imageMat.channels() == 1) {
        return image;
    }
    
    cv::cvtColor(imageMat, imageMat, CV_RGBA2GRAY);
    
    return MatToUIImage(imageMat);
}

+ (UIImage *) detectBordersOfImage:(UIImage *)image withThreshold:(float)value
{
    cv::Mat imageMat;
    UIImageToMat(image, imageMat);
    
    cv::GaussianBlur(imageMat, imageMat, cv::Size(3, 3), 0);
    
    cv::Canny(imageMat, imageMat, value, value);
    
    return MatToUIImage(imageMat);
}

+ (UIImage *) detectFacesOfImage: (UIImage *)image
{
    cv::Mat imageMat;
    cv::CascadeClassifier faceDetector;
    
    UIImageToMat(image, imageMat);
    
    NSString *cascadePath = [[NSBundle mainBundle] pathForResource:@"haarcascade_frontalface_alt2" ofType:@"xml"];
    faceDetector.load([cascadePath UTF8String]);
    
    std::vector<cv::Rect> faces;
    faceDetector.detectMultiScale(imageMat, faces);
    
    for(unsigned int i = 0; i < faces.size(); i++)
    {
        const cv::Rect& face = faces[i];
        // Get top-left and bottom-right corner points
        cv::Point tl(face.x, face.y);
        cv::Point br = tl + cv::Point(face.width, face.height);
        // Draw rectangle around the face
        cv::Scalar magenta = cv::Scalar(255, 0, 255);
        cv::rectangle(imageMat, tl, br, magenta, 4, 8, 0);
    }
    
    return MatToUIImage(imageMat);
}

-(id)initWithController:(VideoViewController*)c andImageView:(UIImageView*)iv
{
    viewController = c;
    imageView = iv;
    threshold = 50;
    
    videoCamera = [[CvVideoCamera alloc] initWithParentView:imageView];
    // ... set up the camera
    
    videoCamera.delegate = self;
    videoCamera.defaultAVCaptureDevicePosition = AVCaptureDevicePositionBack;
    videoCamera.defaultAVCaptureSessionPreset = AVCaptureSessionPreset640x480;
    videoCamera.defaultAVCaptureVideoOrientation = AVCaptureVideoOrientationLandscapeLeft;
    videoCamera.defaultFPS = 30;
    videoCamera.rotateVideo = !videoCamera.rotateVideo;
    
    [videoCamera start];
    return self;
}

- (void) changeThreshold:(float) value {
    threshold = value;
}

- (void)processImage:(cv::Mat&)image
{
    //cv::cvtColor(image, image, CV_RGBA2GRAY);
    
    // TODO - Procesa en tiempo real la detección de bordes usando el método de Canny (realizar un suavizado de  la imagen previo a llamar al método Canny)
    cv::GaussianBlur(image, image, cv::Size(3, 3), 0);
    cv::Canny(image, image, threshold, threshold);
    // TODO - Añade dos sliders, uno para modificar en vivo la intensidad del suavizado de la imagen y otro para los umbrales del detector de bordes
    
    // TODO añade otro tipo de funciones de OpenCV
}


@end
