//
//  OpenCVWrapper.h
//  openCV
//
//  Created by Master Móviles on 17/11/16.
//  Copyright © 2016 Master Móviles. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

#ifdef __cplusplus

#import <opencv2/opencv.hpp>
#import "opencv2/highgui/ios.h"
#import <opencv2/highgui/cap_ios.h>

#endif

@class VideoViewController;

@interface OpenCVWrapper : NSObject

- (id) initWithController:(VideoViewController*) c andImageView:(UIImageView *) iv;
- (void) changeThreshold:(float) value;

+ (UIImage *) convertImageToGrayscale:(UIImage*) image;
+ (UIImage *) detectBordersOfImage:(UIImage *) image withThreshold:(float) value;
+ (UIImage *) detectFacesOfImage:(UIImage *) image;

@end
