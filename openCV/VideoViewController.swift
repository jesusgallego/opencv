//
//  VideoViewController.swift
//  openCV
//
//  Created by Master Móviles on 17/11/16.
//  Copyright © 2016 Master Móviles. All rights reserved.
//

import UIKit

class VideoViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    
    var openCVWrapper: OpenCVWrapper!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        openCVWrapper = OpenCVWrapper(controller: self, andImageView: imageView);
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onSliderChange(_ sender: Any) {
        let slider = sender as! UISlider
        openCVWrapper.changeThreshold(slider.value)
    }
    

}
